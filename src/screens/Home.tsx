import React from 'react';
import {StyleSheet, View} from 'react-native';
import Button from '../components/Button';

export default function App() {
  return (
    <View style={styles.container}>
      <Button />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
});
