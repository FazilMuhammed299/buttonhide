import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  Animated,
  TouchableWithoutFeedback,
} from 'react-native';
export default function Button() {
  const [visible, setVisible] = useState(true);

  const animatedButtonScale = new Animated.Value(1);

  // When button is pressed in, animate the scale to 1.5
  const onPressIn = () => {
    Animated.spring(animatedButtonScale, {
      toValue: 1.5,
      useNativeDriver: true,
    }).start();
  };

  // When button is pressed out, animate the scale back to 1
  const onPressOut = () => {
    Animated.spring(animatedButtonScale, {
      toValue: 1,
      useNativeDriver: true,
    }).start();

    setTimeout(() => {
      onPress();
    }, 500);
  };

  // The animated style for scaling the button within the Animated.View
  const animatedScaleStyle = {
    transform: [{scale: animatedButtonScale}],
  };

  const onPress = () => {
    setVisible(false);
  };
  if (!visible) {
    return null;
  }
  return (
    <TouchableWithoutFeedback
      onPress={() => {}}
      onPressIn={onPressIn}
      onPressOut={onPressOut}>
      <Animated.View style={[styles.button, animatedScaleStyle]}>
        <Text style={styles.text}>Click me</Text>
      </Animated.View>
    </TouchableWithoutFeedback>
  );
}
const styles = StyleSheet.create({
  button: {
    backgroundColor: '#4CAF50',
    borderRadius: 5,
    padding: 10,
  },
  text: {
    color: '#FFFFFF',
    fontSize: 16,
    textAlign: 'center',
  },
});
